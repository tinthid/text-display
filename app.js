const express = require('express')
const path = require('path')
const app = express()

var secret = 'REDACTED'
var users = []

function createUser(username, password, text) {
    if( username === null )
        return {}
    return {
        username,
        password,
        text
    }
}

function deepJoin(t, s) {
    for (let k in s) {
        if ((typeof t[k] === 'function' || typeof t[k] === 'object') &&
            (typeof s[k] === 'function' || typeof s[k] === 'object')) {
                deepJoin(t[k], s[k])
        } else {
            t[k] = s[k]
        }
    }
    return t
}

users.push(
    createUser('Guest',
        '123456',
        'It’s not a bug – it’s an undocumented feature.')
    )

users.push(
    createUser('John',
        'password',
        'It works on my machine.')
    )


template = `
<html>
    <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Space+Mono&display=swap" rel="stylesheet">
    </head>
    <body>
        $body
    </body>
    <style>
        body {
            font-family: 'Space Mono', monospace;
        }
    </style>
</html>
`
app.get('/', (req, res) => {

    let html = template.replace('$body', `
        Hi :) <br>
        Welcome, everybody! <br>
        <a href="?data=%7B%22password%22%3A123456%7D"> Guest Login </a>
        <!-- https://gitlab.com/tinthid/text-display -->
    `)
    
    if (req.query.data !== undefined) {
        let input = deepJoin(createUser(null, null, null, null),
            JSON.parse(req.query.data))
        for (let k in users) {
            if (users[k].password == input.password) {
                if (users[k].isAdmin !== undefined) {
                    html = template.replace('$body', `
                        Hello, Admin :) <br>
                        ${secret} <br>
                    `)

                    /*

                    HIDDEN CODE
                    BUT YOU DON'T NEED THIS PART TO GET THE SECRET
                    
                    */

                } else {
                    html = template.replace('$body', `
                        Hello, ${users[k].username} :) <br>
                        ${users[k].text} <br>
                    `)
                }
                break
            }
        }
    }
    
    res.set('Content-Type', 'text/html');
    res.send(Buffer.from(html));
})

app.listen(process.env.PORT || 3000, () => {
    console.log(`App is listening at http://localhost:${process.env.PORT || 3000}`)
})
